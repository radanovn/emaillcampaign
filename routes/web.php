<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('contacts', 'ContactController@index');

Route::get('contacts/create', 'ContactController@create');

Route::get('/contacts/{id}/delete','ContactController@destroy')->name('contacts.destroy');

Route::post('contactsaction', 'ContactController@storeContact');

Route::get('contacts/{id}/edit', 'ContactController@edit')->name('contacts.edit');

Route::get('contacts/{id}/destroy', 'ContactController@destroy')->name('contacts.destroy');

Route::post('contacts/{id}/update','ContactController@update')->name('contacts.update');

Route::get('contacts/export', 'ContactController@export')->name('contacts.export');

Route::get('importExportView', 'ContactController@importExportView');

Route::post('contacts/import', 'ContactController@import')->name('contacts/import');

Route::post('contacts/addtogroup', 'GroupController@store');

Route::get('contacts/createGroup', 'GroupController@createGroup')->name('contacts/group');

Route::get('contacts/groups', 'GroupController@showGroups');

Route::get('groups/{id}/editGroup', 'GroupController@edit')->name('groups.editGroup');

Route::get('groups/{id}/destroy', 'GroupController@destroy')->name('groups.destroy');

Route::post('groups/{id}/update','GroupController@update')->name('groups.update');


// Campaigns
Route::resource('campaigns', 'CampaignController');