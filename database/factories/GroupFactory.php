<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Group;
use Faker\Generator as Faker;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

$factory->define(Group::class,  function (Faker $faker)
{
    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'group_name' => $faker->name,

    ];

});
