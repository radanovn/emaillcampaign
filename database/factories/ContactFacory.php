<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ContactModel;
use Faker\Generator as Faker;

$factory->define(ContactModel::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'firstname' => $faker->name,
        'lastname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,

    ];
});



