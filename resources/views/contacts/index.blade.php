@extends('layouts.master')
@section('title','contacts Index')
@section('content')


<br>
<hr>
<div class="row">
  <div class="col-sm-12">
    <table class="table">
      <tr class="text-center">
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
      </tr>
      @foreach($contacts as $contact)
      <tr class="text-center">
        <td>{{ $contact->firstname }}</td>
        <td>{{ $contact->lastname }}</td>
        <td>{{ $contact->email }}</td>
        <td><a href="{{route('contacts.edit',$contact->id)}}" class="btn btn-info">Edit</a></td>
        <td><a href="{{route('contacts.destroy',$contact->id)}}" class="btn btn-danger">Delete</a></td>
      </tr>
      @endforeach
    </table>
  </div>


  <br>
</div>



@endsection