@extends('layouts.master')
@section('title','contacts Index')
@section('content')
<div class="row">
  <div class="col-sm-12">
    <table class="table">
      <tr class="text-center">
        <th>Group Name</th>
      </tr>
      @foreach($groups as $group)
      <tr class="text-center">
        <td>{{ $group->group_name }}</td>
        <td><a href="{{route('groups.editGroup',$group->id)}}" class="btn btn-info">Edit</a></td>
        <td><a href="{{route('groups.destroy',$group->id)}}" class="btn btn-danger">Delete</a></td>
      </tr>
      @endforeach
    </table>
  </div>
  <br>
</div>



@endsection