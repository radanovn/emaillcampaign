@extends('layouts.master')
@section('title','Create Contact')
@section('content')
  <div class="row mt-5">
    <div class="col-sm-8 offset-sm-2">
      <form action="/contactsaction" method = "post">
        @csrf
        <div class="form-group">
          <label for="firstname">Firstname:</label>
          <input type="text" name = "firstname" id = "firstname" class="form-control" required>
        </div>
        <div class="form-group">
          <label for="lastname">Lastname:</label>
          <input type="text" name = "lastname" id = "lastname" class="form-control" required>
        </div>
        <div class="form-group">
          <label for="email">Email:</label>
          <input type="email" name = "email" id = "email" class="form-control" required>
        </div>
        
        
        <button type = "submit" class = "btn btn-success">Submit</button> OR <a class='btn btn-success' href="/importExportView">Import file</a>
      </form>
    </div>
  </div>
@endsection
