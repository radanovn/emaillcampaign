@extends('layouts.master')
@section('title','Edit group')
@section('content')
<div class="row">
  <div class="col-sm-8 offset-sm-2">

    <form action="{{route('groups.update', $group->id)}}" method="POST">
      @csrf
      <div class="form-group">
        <label for="group_name">Firstname:</label>
        <input type="text" name="group_name" id="group_name" class="form-control" required value="{{$group->group_name}}">
      </div>
      <label for="groupname">Select from contacts:</label>
      <select class="js-example-basic-multiple" name="contacts[]" multiple="multiple">
        @foreach($contacts as $contact)
        <option value="{{ $contact->id }}">{{ $contact->firstname }} {{ $contact->lastname }}
        </option>
        @endforeach
      </select>


      <input type="hidden" name="id" value="{{$group->id}}">
      <button type="submit" class="btn btn-success">Edit</button>
    </form>
  </div>
</div>
@endsection