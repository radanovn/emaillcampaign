@extends('layouts.master')
@section('title','Create Group')
@section('content')
<div class="row mt-5">
    <div class="col-sm-8 offset-sm-2">
        <form action="addtogroup" method="POST">
            @csrf
            <div class="form-group">
                <label for="group_name">Group name:</label>
                <input type="text" name="group_name" id="group_name" class="form-control" required>
            </div>



            <label for="groupname">Select from contacts:</label>
            <select class="js-example-basic-multiple" name="contacts[]" multiple="multiple">
                @foreach($contacts as $contact)
                <option value="{{ $contact->id }}">{{ $contact->firstname }} {{ $contact->lastname }}
                </option>
                @endforeach
            </select>
            <hr>
            <button type="submit" class="btn btn-success">Create</button>

        </form>
    </div>
</div>



@endsection