@extends('layouts.master')
@section('title','Edit contact')
@section('content')
  <div class="row">
    <div class="col-sm-8 offset-sm-2">
     
    <form action="{{route('contacts.update', $contact->id)}}" method = "post">
        @csrf
        <div class="form-group">
          <label for="firstname">Firstname:</label>
          <input type="text" name = "firstname" id = "firstname" class="form-control" required value = "{{$contact->firstname}}">
        </div>
        <div class="form-group">
          <label for="lastname">Lastname:</label>
          <input type="text" name = "lastname" id = "lastname" class="form-control" required value = "{{$contact->lastname}}">
        </div>
        <div class="form-group">
          <label for="email">email:</label>
          <input type="email" name = "email" id = "email" class="form-control" required value = "{{$contact->department}}">
        </div>
        
        <input type="hidden" name="id" value = "{{$contact->id}}">
        <button type = "submit" class = "btn btn-success">Submit</button>
      </form>
    </div>
  </div>
@endsection