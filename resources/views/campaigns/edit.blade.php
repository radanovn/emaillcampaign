@extends('layouts.master')
@section('title','Create Contact')
@section('content')
<div class="row mt-5">
  <div class="col-sm-8 offset-sm-2">
    <form action="{{ route('campaigns.update' , $campaign->id) }}" method="POST" enctype="multipart/form-data" data-redirect="{{ route('campaigns.index') }}">
      @csrf
      @method('PUT')

      <div class=" form-group">
      <label>Name:</label>
      <input type="text" name="name" id="name" class="form-control" required>
  </div>
  <div class="form-group">
    <label>From (Email):</label>
    <input type="email" name="from" id="from" class="form-control" required>
  </div>
  <div class="form-group">
    <label>Subject</label>
    <input type="text" name="subject" id="subject" class="form-control" required>
  </div>

  <div class="form-group">
    <label>Title:</label>
    <input type="text" name="title" id="title" class="form-control" required>
  </div>

  <div class="form-group">
    <label>Text</label>
    <input type="text" name="button_text" id="button_text" class="form-control" required>
  </div>

  <div class="form-group">
    <label>Color:</label>
    <input type="color" name="button_color" id="button_color" class="form-control" required>
  </div>

  <div class="form-group">
    <label>Logo:</label>
    <input type="file" accept="png, jpg, jpeg, svg, gif" name="logo" id="logo" class="form-control" required>
  </div>



  <div class="form-group">
    <h5>Choose recipient:</h5>
  </div>

  <div class="form-group">
    <label for="group">Choose contacts from group:</label>

    <select id="group" class="form-control" style="width: 100%; margin-bottom: 20px;">
      @foreach($groups as $group)

      <option value="{{ $group->id }}">{{ $group->group_name }}</option>
      @endforeach
    </select>

  </div>


  <button type="submit" class="btn btn-success">Submit</button>
  </form>

</div>
@endsection