@extends('layouts.master')
@section('title','contacts Index')
@section('content')
<hr>

<br>
<hr>
<div class="row">
  <div class="col-sm-12">
    <table class="table">
      <tr class="text-center">
        <th>Name</th>
        <th>From</th>
        <th>Subject</th>
        <th>Аctions</th>
      </tr>

      @foreach($campaigns as $campaign)
      <tr class="text-center">
        <td>{{ $campaign->name }}</td>
        <td>{{ $campaign->from }}</td>
        <td>{{ $campaign->subject }}</td>
        <td><a href="{{ route('campaigns.edit', $campaign->id )}}" class="btn btn-info">Edit</a></td>
        <td>
          <form class="delete" action="{{ route('campaigns.destroy', $campaign->id) }}" method="POST">
            <input type="hidden" name="_method" value="DELETE">
            {{ csrf_field() }}
            <input type="submit" value="Delete">
          </form>
        </td>
      </tr>
      @endforeach
    </table>
  </div>


  <br>
</div>

<script>
    $(".delete").on("submit", function(){
        return confirm("Are you sure?");
    });
</script>

@endsection