<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Contacts\StoreContactRequest;
use App\Http\Requests\Contacts\UpdateContactRequest;
use App\ContactModel;
use App\Exports\ContactExport;
use App\Imports\ContactImport;
use Illuminate\Support\Facades\Auth;
use Contacts;
use Mail;
use Maatwebsite\Excel\Facades\Excel;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $contacts = ContactModel::all();

        return view('contacts.index', compact('contacts', 'groups'));
    }

    public function create()
    {
        return view('contacts.create');
    }

    public function storeContact(StoreContactRequest $request)
    {

        $contact = new ContactModel();
        $contact->user()->associate(Auth::user());

        $contact->firstname = $request->firstname;
        $contact->lastname = $request->lastname;
        $contact->email = $request->email;

        $contact->save();

        return redirect('/contacts')->with('Success', 'Contact added successfully');
    }

    public function update(Request $request)
    {

        $contact = ContactModel::find($request->input('id'));
        $contact->firstname = $request->input('firstname');
        $contact->lastname = $request->input('lastname');
        $contact->email = $request->input('email');
        $contact->save();
        return redirect('/contacts')->with('Success', 'Contact updated Successfully');
    }

    public function edit($id)
    {
        $contact = ContactModel::find($id);
        return view('contacts.edit', compact('contact'));
    }

    public function destroy($id)
    {

        $contact = ContactModel::find($id);

        $contact->delete();
        return redirect('/contacts');
    }

    public function handleForm(Request $request)
    {
        $this->validate($request, ['firstname' => 'required', 'lastname' => 'required', 'email' => 'required|email']);

        $data = ['firstname' => $request->get('username'), 'lastname' => $request->get('lastname'), 'email' => $request->get('email')];

        Mail::send('email', $data, function ($message) use ($data) {
            $message->from($data['email'], $data['firstname'], $data['lastname']);
        });
    }


    public function export()
    {
        return Excel::download(new ContactExport, 'contacts.xlsx');
    }

    public function import(Request $request)
    {
        Excel::import(new ContactImport, $request->file);

        return redirect('/contacts')->with('Success', 'Done!');
    }

    public function importExportView()
    {
        return view('contacts/import');
    }
}
