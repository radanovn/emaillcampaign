<?php

namespace App\Http\Controllers;

use App\Campaign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // Hashing function for the image name
    public function hashImageName($image)
    {
        return md5($image->getClientOriginalName() . uniqid('weband')) . '.' . $image->getClientOriginalExtension();
    }

    /**
     * CampaignController constuctor
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     * @return \illuminate\Http\Response
     */
    public function index()
    {
        $campaigns = Auth::user()->campaigns;

        return view('campaigns.index', compact('campaigns'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $contacts = Auth::user()->contacts;
        $groups = Auth::user()->groups;

        return view('campaigns.create', compact('groups', 'contacts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $campaign = new Campaign;
        $campaign->user()->associate(Auth::user());
        $campaign->name = $request->name;
        $campaign->from = $request->from;
        $campaign->subject = $request->subject;
        $campaign->title = $request->title;
        $campaign->text = $request->text;
        $campaign->button_text = $request->button_text;
        $campaign->button_color = $request->button_color;

        //Check if image is presented in the request and it's valid
        if ($request->hasFile('logo') && $request->logo->isvalid()) {
            $logo = $request->logo;
            $image_name = $this->hashImageName($logo);

            //storage the image in filesystem
            Storage::disk('local')->putFileAs('logos', $logo, $image_name);

            //Storage the path as thext in the database
            $campaign->logo = $image_name;
        }

        $campaign->save();

        $campaign->contacts()->attach($request->contacts);

        return redirect()->route('campaigns.index')->with('Success', 'Contact added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function show(Campaign $campaign)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function edit(Campaign $campaign)
    {
      
        $contacts = Auth::user()->contacts;
        $groups = Auth::user()->groups;

        return view('campaigns.edit', compact('campaign', 'contacts', 'groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Campaign $campaign)
    {

        $campaign->name = $request->name;
        $campaign->from = $request->from;
        $campaign->subject = $request->subject;
        $campaign->title = $request->title;
        $campaign->text = $request->text;
        $campaign->button_text = $request->button_text;
        $campaign->button_color = $request->button_color;

        if ($request->hasFile('logo') && $request->logo->isValid()) {
            $logo = $request->logo;
            $image_name = $this->hashImageName($logo);

            //storage the image in filesystem
            Storage::disk('local')->putFileAs('logos', $logo, $image_name);


            //Storage the path as thext in the database
            
            
            $campaign->deleteLogo();
            
            $campaign->logo = $image_name;
            
            
        }
        
        $campaign->save();
        $campaign->contacts()->sync($request->contacts);
        
        return redirect()->route('campaigns.index')->with('Success', 'Contact added successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function destroy(Campaign $campaign)
    {
        $campaign->deleteLogo();
        $campaign->delete();

        return redirect()->back()->with('success', 'Кампанията беше изтрита');
    }
}
