<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;
use App\ContactModel;
use App\Http\Requests\Groups\AddContactsRequest;
use App\Http\Requests\Groups\StoreGroupRequest;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showGroups()
    {
        $groups = Group::all();

        return view('contacts.showGroups', compact('groups'));
    }

    public function createGroup()
    {

        $contacts = Auth::user()->contacts;

        return view('contacts.group', compact('contacts'));
    }

    public function store(StoreGroupRequest $request)
    {
        $group = new Group();

        $group->user()->associate(Auth::user());

        $group->group_name = $request->group_name;

        $group->save();

        $group->contacts()->attach($request->contacts);

        return redirect('contacts/groups')->with('Success', 'Group added successfully');
    }

    public function addContacts(AddContactsRequest $request)
    {
        $group = Group::find($request->group);

        $group->contacts()->attach($request->contacts);

        return redirect()->route('/contacts/groups')->wuth('success', 'The contacts were added');
    }


    public function update(Request $request, Group $group)
    {
        
        $group = Group::find($request->input('id'));
        $group->group_name = $request->input('group_name');
        
        $group->save();

        $group->contacts()->sync($request->contacts);

        return redirect('contacts/groups')->with('Success', 'Contact updated Successfully');
    }

    public function edit($id)
    {
        $group = Group::find($id);
        $contacts = Auth::user()->contacts;
        
        return view('contacts.editGroup', compact('group', 'contacts'));
    }

    public function destroy($id)
    {

        $group = Group::find($id);

        $group->delete();
        return redirect('contacts/groups');
    }


    public function getContacts(Group $group)
    {
        return response()->json($group->contacts);
    }
}
