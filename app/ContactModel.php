<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ContactModel extends Model
{

    protected $table = 'contacts';

    protected $fillable = ['user_id', 'firstname', 'lastname', 'email'];



    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */

    public function user()
    {
        return $this->belongsTo('App\User')->withDefault([
            'name' => '-'
        ]);
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group', 'contact_user', 'contact_id');
    }
}
