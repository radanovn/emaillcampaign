<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{

    protected $fillable = ['user_id', 'group_name'];


    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */

    public function user()
    {
        return $this->belongsTo('App\User')->withDefault([
            'name' => '-'
        ]);
    }

    public function contacts()
    {
        return $this->belongsToMany('App\ContactModel', 'contact_user', 'group_id', 'contact_id');
    }
}
