<?php

namespace App;

use Illuminate\Support\Facades\Storage;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    //Relations
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function contacts()
    {
        return $this->belongsToMany('App\ContactModel', 'compaign_contact');

    }

    // Helpers
    public function deleteLogo()
    {
        if (Storage::disk('local')->exists('logos/'.$this->image)) {
            Storage::disk('local')->delete('logos/'.$this->image);
        }

        return true;
    }
}

